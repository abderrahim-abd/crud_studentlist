<?php

namespace app\services;
use app\models\StudentModel;

class StudentService
{
    private $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function addStudent(StudentModel $student)
    {
        $sql = "INSERT INTO students (first_name, last_name, branch, email, age, phone_number) 
                VALUES (:firstName, :lastName, :branch, :email, :age, :phoneNumber)";

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(':firstName', $student->getFirstName());
        $stmt->bindValue(':lastName', $student->getLastName());
        $stmt->bindValue(':branch', $student->getBranch());
        $stmt->bindValue(':email', $student->getEmail());
        $stmt->bindValue(':age', $student->getAge());
        $stmt->bindValue(':phoneNumber', $student->getPhoneNumber());

        $stmt->execute();

        return $student;
    }

    public function modifyStudent(StudentModel $student)
    {
        $sql = "UPDATE students SET first_name = :firstName, last_name = :lastName, branch = :branch, 
                email = :email, age = :age, phone_number = :phoneNumber WHERE id = :id";

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(':firstName', $student->getFirstName());
        $stmt->bindValue(':lastName', $student->getLastName());
        $stmt->bindValue(':branch', $student->getBranch());
        $stmt->bindValue(':email', $student->getEmail());
        $stmt->bindValue(':age', $student->getAge());
        $stmt->bindValue(':phoneNumber', $student->getPhoneNumber());
        $stmt->bindValue(':id', $student->getId());

        $stmt->execute();

        return $student;
    }

    public function deleteStudent(StudentModel $student)
    {
        $sql = "DELETE FROM students WHERE id = :id";

        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(':id', $student->getId());

        return $stmt->execute();
    }

    public function getAllStudents()
    {
        $sql = "SELECT * FROM students";

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }
}