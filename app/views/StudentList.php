<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Student List</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../public/css/StudentList.css" rel="stylesheet">
</head>
<body>
<nav class="navbar bg-body-tertiary">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">
            <img src="../../public/img/png/logosqli.png" alt="Logo" width="60" height="24" class="d-inline-block align-text-top">
            SQLI
        </a>
    </div>
</nav>
<div class="container mt-5">
    <h2 class="text-center mb-4">Student List</h2>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>id</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Branch</th>
            <th>Email</th>
            <th>Age</th>
            <th>Phone Number</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th>1</th>
            <td>John</td>
            <td>Doe</td>
            <td>Réseaux informatique</td>
            <td>john.doe@example.com</td>
            <td>21</td>
            <td>0617565676</td>
            <td>
                <button type="button" class="btn btn-primary btn-sm">Modify</button>
                <button type="button" class="btn btn-danger btn-sm">Delete</button>
            </td>
        </tr>
        <tr>
            <th>2</th>
            <td>Jane</td>
            <td>Smith</td>
            <td>Systems informatique</td>
            <td>jane.smith@example.com</td>
            <td>22</td>
            <td>0617564787</td>
            <td>
                <button type="button" class="btn btn-primary btn-sm">Modify</button>
                <button type="button" class="btn btn-danger btn-sm">Delete</button>
            </td>
        </tr>
        </tbody>
    </table>
    <div class="text-center">
        <button type="button" class="btn btn-success">Add Student</button>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.min.js"></script>
</body>
</html>

