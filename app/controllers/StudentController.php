<?php


namespace app\controllers;

use app\services\StudentService;
use app\models\StudentModel;

class StudentController
{
    private $studentService;

    public function __construct(StudentService $studentService)
    {
        $this->studentService = $studentService;
    }

    public function showStudentList()
    {
        // Fetch all students from the service
        $students = $this->studentService->getAllStudents();

        // Render the student list view and pass the student data to it
        $studentListView = new StudentList($this->studentController);
        $studentListView->render($students);
    }

    public function addStudent(StudentModel $student)
    {
        // Call the service function to add the student
        $this->studentService->addStudent($student);

        // Redirect to the student list page after adding
        header("Location: StudentList.php");
        exit();
    }

    public function modifyStudent(StudentModel $student)
    {
        // Call the service function to modify the student
        $this->studentService->modifyStudent($student);

        // Redirect to the student list page after modifying
        header("Location: StudentList.php");
        exit();
    }

    public function deleteStudent(StudentModel $student)
    {
        // Call the service function to delete the student
        $this->studentService->deleteStudent($student);

        // Redirect to the student list page after deleting
        header("Location: StudentList.php");
        exit();
    }
}
