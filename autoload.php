<?php

spl_autoload_register(function ($className) {
    $rootDirectory = __DIR__;

    $classFile = str_replace('\\', '/', $className) . '.php';

    $classPath = $rootDirectory . '/' . $classFile;

    if (file_exists($classPath)) {
        require_once $classPath;
    }
});
